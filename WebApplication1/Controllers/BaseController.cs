﻿using EventBus.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.IntegrationEvents;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        private readonly IEventBus _eventBus;
        public BaseController(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }
        [HttpGet]
        public string Get()
        {
            return "Appllication1";
        }
        [HttpGet("pubsub")]
        public void Subscrib(string content)
        {
            TestEvent testEvent = new TestEvent(content);
            _eventBus.PublishAsync(testEvent);
        }
    }
}
