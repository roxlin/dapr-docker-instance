﻿using System.Net.Http.Headers;

namespace GatwayService.Services
{
    public class App1Service:IApp1Service
    {
        private readonly HttpClient _httpClient;

        public App1Service(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<string> UpdateAsync(string content, string accessToken)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, "api/base")
            {
                Content = JsonContent.Create(content)
            };

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            //post
            //var response = await _httpClient.SendAsync(request);
            //response.EnsureSuccessStatusCode();

            //get
            var response = await _httpClient.SendAsync(request);
            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
