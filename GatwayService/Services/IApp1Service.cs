﻿namespace GatwayService.Services
{
    public interface IApp1Service
    {
        Task<string> UpdateAsync(string content, string accessToken);
    }
}
