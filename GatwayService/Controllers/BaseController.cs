﻿using GatwayService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GatwayService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        private readonly IApp1Service _app1;
        public BaseController(IApp1Service app1)
        {
            _app1 = app1;
        }
        [HttpGet]
        public string Get()
        {
            return "Gateway";
        }
        [HttpGet("app")]
        public async Task<string> Application(string tag)
        {
            using (var client = new HttpClient())
            {
                string uri = "";
                if (tag == "1") uri = "http://webapplication1/api/base";
                else uri = "http://webapplication2/api/base";
                var result = await client.GetStringAsync(uri);
                return result;
            }
        }
        [HttpGet("pubsub")]
        public async Task<string> pubsub(string tag)
        {
            using (var client = new HttpClient())
            {
                string uri = $"http://webapplication1/api/base/pubsub?content={tag}";
                var result = await client.GetStringAsync(uri);
                return result;
            }
        }
        [HttpGet("app1")]
        public async Task<string> App1(string centent="Application")
        {
            return await _app1.UpdateAsync(centent, "Bear");
        }
    }
}
