﻿using Dapr;
using Dapr.Actors;
using Dapr.Actors.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication2.Actors;
using WebApplication2.IntegrationEvents;

namespace WebApplication2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        private const string DAPR_PUBSUB_NAME = "pubsub";

        private readonly IActorProxyFactory _actorProxyFactory;
        private readonly ILogger<BaseController> _logger;

        public BaseController(IActorProxyFactory actorProxyFactory,
        ILogger<BaseController> logger)
        {
            _actorProxyFactory = actorProxyFactory;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public async Task<string> Get()
        {
            return "Appllication2";
        }

        [HttpGet("act")]
        public async Task<string> GetAct(string centent)
        {
            var actorId = new ActorId(Guid.NewGuid().ToString());
            return await Task.Run(() => { return _actorProxyFactory.CreateActorProxy<IApp2Actor>(
                actorId,
                nameof(App2Actor)).Get(centent);
            }); 
        }
        [HttpPost("pubsub")]
        [Topic("pubsub", "testevent")]
        public async Task GetPubSub(TestEvent @event)
        {
            await Task.Run(() => { Console.WriteLine(@event.content); });
            
            //return @event.content;
        }
    }
}
